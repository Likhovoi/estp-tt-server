type Callback<T> = (...args: any[]) => Promise<T>;

/**
 * Decorates function to guarantee that each new its call starts executing only 
 * when every async operations of its previous call is finished.
 * 
 * @param cb Callback
 */
export default function<T> (cb: Callback<T>) {
    type Task = {
        args: any[];
        resolve: (r: T) => void,
        reject: (e: Error) => void,
    };

    let processing = false;
    const queue: Task[] = [];

    return async function (...args: any[]) {
        return new Promise<T>(async (resolve, reject) => {
            queue.push({ args, resolve, reject });

            const execute = async () => { // executes tasks while queue is not empty
                const task = queue.shift();

                if (!task) {
                    return;
                }

                try {
                    const taskRes = await cb(...task.args);
                    task.resolve(taskRes);
                } catch (taskErr) {
                    task.reject(taskErr);
                }

                if (queue.length) {
                    await execute();
                }
            };

            if (!processing) {
                processing = true;
                await execute();
                processing = false;
            }
        });
    }
}
