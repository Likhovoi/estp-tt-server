export interface ListStorage<T> {
    push(data: T): Promise<number>;
    getAll(): Promise<T[]>;
}

export interface Transaction {
    readonly value: number;
    readonly createdAt: number;
    readonly currentValue: number;
}
