import Router from 'koa-router';
import { Context } from 'koa';
import { NextFunction } from 'connect';
import { IsNumber, IsPositive, validate } from 'class-validator';
import { ChanageBalanceError } from './change-balance.error';
import { TransactionManager } from './transaction.manager';
import { ListStorage, Transaction } from './app.contracts';

class BalanceDTO {
    @IsNumber()
    @IsPositive()
    value!: number;
}

const createDTO = async (ctx: Context) => {
    let dto = new BalanceDTO();
    dto = Object.assign(dto, ctx.request.body);
    const errors = await validate(dto);
    if (errors.length) {
        ctx.throw(412, JSON.stringify(errors));
    }
    return dto;
};

const changeBalanceAction = async (transactionWorker: TransactionManager, ctx: Context, next: NextFunction, value: number) => {
    try {
        const transaction = await transactionWorker.changeBalance(value);
        ctx.body = transaction;
        return next();
    } catch (e) {
        if (e instanceof ChanageBalanceError) {
            ctx.throw(400);
        } else {
            ctx.throw(500);
        }
    }
}

export default (transactionWorker: TransactionManager, router: Router, storage: ListStorage<Transaction>) => {
    router.post('/debit', async (ctx: Context, next: NextFunction) => {
        const dto = await createDTO(ctx);
        return await changeBalanceAction(transactionWorker, ctx, next, dto.value);
    });
    
    router.post('/credit', async (ctx: Context, next: NextFunction) => {
        const dto = await createDTO(ctx);
        return await changeBalanceAction(transactionWorker, ctx, next, -1 * dto.value);
    });
    
    router.get('/history', async (ctx: Context, next: NextFunction) => {
        try {
            const transactions = await storage.getAll();
            ctx.body = transactions;
            return next();
        } catch (e) {
            ctx.throw(500);
        }
    });
};
