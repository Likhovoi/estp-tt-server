export class ChanageBalanceError extends Error {
    constructor(message: string) {
        super(message);
        Error.captureStackTrace(this, ChanageBalanceError);
        Object.setPrototypeOf(this, ChanageBalanceError.prototype);
    }
}
