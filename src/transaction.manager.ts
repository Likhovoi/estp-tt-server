import { ListStorage, Transaction } from "./app.contracts";
import { ChanageBalanceError } from "./change-balance.error";
import atomic from './atomic.util';

/**
 * Provides operations available on transactions list
 */
export class TransactionManager {
    private storage: ListStorage<Transaction>;
    private lastTransaction: Transaction|null;

    constructor(storage: ListStorage<Transaction>, lastTransaction: Transaction|null = null) {
        this.storage = storage;
        this.lastTransaction = lastTransaction;
    }

    public changeBalance = atomic<Transaction>(async (value: number) => {
        const currentValue = this.lastTransaction ? this.lastTransaction.currentValue : 0;
        const nextValue = currentValue + value;

        if (nextValue < 0) {
            throw new ChanageBalanceError('Invalid argument passed');
        }

        const nextTransaction: Transaction = {
            value,
            currentValue: nextValue,
            createdAt: (new Date()).getTime()
        };

        await this.storage.push(nextTransaction);
        this.lastTransaction = nextTransaction;
        return nextTransaction;
    })
}
