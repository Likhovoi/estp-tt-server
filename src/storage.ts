import { ListStorage, Transaction } from './app.contracts';
import { RedisClient } from 'redis';

export class RedisListStorage<T> implements ListStorage<T> {
    private driver: RedisClient;
    private listName: string;
    private encode = JSON.stringify;
    private decode = JSON.parse;

    constructor(driver: RedisClient, listName: string) {
        this.driver = driver;
        this.listName = listName;
    }

    push(transaction: T): Promise<number> {
        const strData: string = this.encode(transaction);
        return new Promise<number>((resolve, reject) => {
            this.driver.rpush(this.listName, strData, (err, index) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(index);
                }
            });
        });
    }

    getAll(): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            this.driver.lrange(this.listName, 0, -1, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data.map(item => this.decode(item)));
                }
            });
        });
    }
}
