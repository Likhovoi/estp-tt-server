import Koa from 'koa';
import * as redis from 'redis';
import { RedisListStorage } from './storage';
import Router from 'koa-router';
import attachRoutes from './routes';
import bodyParser from 'koa-bodyparser';
import { TransactionManager } from './transaction.manager';
import { Transaction } from './app.contracts';
import * as dotenv from 'dotenv';

dotenv.config()

const env = Object.assign({
    REDIS_URL: '//127.0.0.1:6379',
    APP_PORT: 3000,
}, process.env);

const app = new Koa();
const router = new Router();
const redisClient = redis.createClient(env.REDIS_URL);
const transactionsStore = new RedisListStorage<Transaction>(redisClient, 'transactions');
const transactionManager = new TransactionManager(transactionsStore);

attachRoutes(transactionManager, router, transactionsStore);

app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(env.APP_PORT);
