# Server side of NodeJs Test Task

## Requirements

You need `redis` to be installed or run application inside docker container created using Dockerfile. 

NOTE: If you choose second option don't forget to start the `redis-server` service.

## Environment

Application depends on 2 environment variables, you can override their values by specifying before running npm command

`REDIS_URL` - address of redis url (127.0.0.1:6379 by default)

`APP_PORT` - address of api (3000 by default)

## Run

To run application in development mode type in console

`npm run dev`

To run production mode type:

`npm run start`
