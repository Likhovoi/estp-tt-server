import chai from 'chai';
import promisedChai from 'chai-as-promised';
import 'mocha';
import { ListStorage, Transaction } from '../src/app.contracts';
import { TransactionManager } from '../src/transaction.manager';
import { ChanageBalanceError } from '../src/change-balance.error';

chai.use(promisedChai);

const { expect } = chai;

describe('TransactionManager', async () => {
    const createStorage = () => {
        const storage: ListStorage<Transaction> = {
            push(data: Transaction): Promise<number> {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve(); // just emulate delay
                    }, 100);
                });
            },
            getAll(): Promise<Transaction[]> {
                return Promise.resolve([]);
            }
        }
        return storage;
    }

    it('Total balance have to be sum of transactions', async () => {
        const storage = createStorage();
        const manager = new TransactionManager(storage);
        await manager.changeBalance(100);
        await manager.changeBalance(100);
        const lastTransaction = await manager.changeBalance(-10);
        expect(lastTransaction.currentValue).to.equal(190);
    });

    it('Transactions have to be processed sequentially', async () => {
        let storage = createStorage();
        let manager = new TransactionManager(storage);
        manager.changeBalance(100);
        manager.changeBalance(100);
        manager.changeBalance(100);
        manager.changeBalance(100);
        manager.changeBalance(100);
        expect(manager.changeBalance(-1000)).to.eventually.be.rejected.and.be.instanceof(ChanageBalanceError);

        storage = createStorage();
        manager = new TransactionManager(storage);
        manager.changeBalance(200);
        manager.changeBalance(200);
        manager.changeBalance(200);
        manager.changeBalance(200);
        manager.changeBalance(200);
        const lastTransaction = await manager.changeBalance(-1000);
        expect(lastTransaction.currentValue).to.equal(0);
    });
});
