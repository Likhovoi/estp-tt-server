"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = __importDefault(require("chai"));
var chai_as_promised_1 = __importDefault(require("chai-as-promised"));
require("mocha");
var transaction_manager_1 = require("../src/transaction.manager");
var change_balance_error_1 = require("../src/change-balance.error");
chai_1.default.use(chai_as_promised_1.default);
var expect = chai_1.default.expect;
describe('TransactionManager', function () { return __awaiter(_this, void 0, void 0, function () {
    var createStorage;
    var _this = this;
    return __generator(this, function (_a) {
        createStorage = function () {
            var storage = {
                push: function (data) {
                    return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            resolve(); // just emulate delay
                        }, 100);
                    });
                },
                getAll: function () {
                    return Promise.resolve([]);
                }
            };
            return storage;
        };
        it('Total balance have to be sum of transactions', function () { return __awaiter(_this, void 0, void 0, function () {
            var storage, manager, lastTransaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        storage = createStorage();
                        manager = new transaction_manager_1.TransactionManager(storage);
                        return [4 /*yield*/, manager.changeBalance(100)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, manager.changeBalance(100)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, manager.changeBalance(-10)];
                    case 3:
                        lastTransaction = _a.sent();
                        expect(lastTransaction.currentValue).to.equal(190);
                        return [2 /*return*/];
                }
            });
        }); });
        it('Transactions have to be processed sequentially', function () { return __awaiter(_this, void 0, void 0, function () {
            var storage, manager, lastTransaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        storage = createStorage();
                        manager = new transaction_manager_1.TransactionManager(storage);
                        manager.changeBalance(100);
                        manager.changeBalance(100);
                        manager.changeBalance(100);
                        manager.changeBalance(100);
                        manager.changeBalance(100);
                        expect(manager.changeBalance(-1000)).to.eventually.be.rejected.and.be.instanceof(change_balance_error_1.ChanageBalanceError);
                        storage = createStorage();
                        manager = new transaction_manager_1.TransactionManager(storage);
                        manager.changeBalance(200);
                        manager.changeBalance(200);
                        manager.changeBalance(200);
                        manager.changeBalance(200);
                        manager.changeBalance(200);
                        return [4 /*yield*/, manager.changeBalance(-1000)];
                    case 1:
                        lastTransaction = _a.sent();
                        expect(lastTransaction.currentValue).to.equal(0);
                        return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
