FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y redis-server

RUN apt-get update
RUN apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get -y install python build-essential nodejs
RUN node -v

WORKDIR /app
COPY . /app
RUN npm install

EXPOSE 3000
